<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
// use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index']);
// Route::get('/', function () {
//     return 'helo';
// });

Route::get('showuser',[UserController::class,'showUser']);


// class Service
// {
//     //
// }

// Route::get('/s', function (Service $service) {
//     die(get_class($service));
// });
