<?php

namespace App\Listeners;

use App\Events\ViewProfile;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyViewProfile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ViewProfile  $event
     * @return void
     */
    public function handle(ViewProfile $event)
    {
        $event->user;
    }
}
