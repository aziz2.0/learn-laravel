<?php

namespace App\Http\Controllers;

use App\Events\ViewProfile;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function showUser()
    {
        $user = User::inRandomOrder()->first();
        // ViewProfile::dispatch($user);
        return $user;
    }
}
